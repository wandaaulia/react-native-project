/* eslint-disable prettier/prettier */
import React, { useEffect, useState} from 'react';
import {ActivityIndicator, Alert, BackHandler, Button, Dimensions, FlatList, Image, ImageBackground, Linking, PermissionsAndroid, RefreshControl, ScrollView, StatusBar, StyleSheet, Switch, Text, TextInput, ToastAndroid, TouchableOpacity, View} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const App = () =>  {
   const [showingText, setIsShowingText] = useState('Header Baru');
const [switchText, setSwitchText] = useState(true);
const [inputText, setInputText] = useState('');
const [refresh, setRefresh] = useState(false);

const dataObjectFood= [
 {
  'nama' : 'Ikan',
  'harga' : 10000
 },
 {
  'nama' : 'Ayam',
  'harga' : 10000
 },
  {
  'nama' : 'Sapi',
  'harga' : 10000
 },
   {
  'nama' : 'Wortel',
  'harga' : 10000
 },
]

const backAction = () => {
  Alert.alert('Perhatian', 'Apakah anda yakin ingin keluar', [
    {
      text: 'cancel',
      onPress: () => null,
      style: 'cancel'
    },
    {
      text: 'ok',
      onPress: () => BackHandler.exitApp()
    }
  ]);
  return true;
}

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title : 'Ijinkan akses',
          message : 'Ijinkan aplikasi mengakses kamera',
          buttonNeutral : 'Nanti',
          buttonNegative : 'Cancel',
          buttonPositive : 'OK',
        },
      );

      if(granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('permission diberikan');
      } else {
        console.log('permisson tidak diberikan');
      }

    } catch (err) {

    }
  }

 useEffect(() => {
  BackHandler.addEventListener('hardwareBackPress', backAction);

  return () => BackHandler.removeEventListener("hardwareBackPress", backAction);
 }, [])

   return (
    <View style={styles.container}>
    <StatusBar barStyle="light-content" backgroundColor="#2196F3"/>
     <View style={styles.header}> 
      <Text style={styles.textHeader}>
        {showingText}
      </Text>
      </View>

       {/* <TouchableOpacity 
       style={styles.imageContainer}
       onPress= {() => Linking.openURL('https://google.com')}> 
        <Image source={{
          uri: 'https://images.unsplash.com/photo-1662986254244-471393e2e05c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80'} }
        style={styles.image}
        />
      </TouchableOpacity> */}

      <TouchableOpacity style={{ flex: 1 }} onPress={() => requestCameraPermission()}> 
      <ImageBackground source={{
          uri: 'https://images.unsplash.com/photo-1662986254244-471393e2e05c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80'} }
          style={{ flex:1, resizemode: 'cover', justifyContent: 'center', alignItems: 'center' }}
          >
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold'}}> Hello World </Text>
          </ImageBackground>
</TouchableOpacity>

      <FlatList data={dataObjectFood} style={styles.flatListContainer}
      refreshControl={
        <RefreshControl refreshing={refresh} onRefresh={
          () => {
            console.log('refresh');
            setRefresh(false);
          }
        } />
      }
      renderItem={({item, index}) => 
      <TouchableOpacity style={styles.arrContainer}
      onPress={() => ToastAndroid.show(
        item.nama + ' di klik',
        ToastAndroid.SHORT
      )}> 
        <Text> {item.nama} </Text>
      <Text> {item.harga} </Text>
      </TouchableOpacity>
      }
      keyExtractor={(item) => item.nama} />

      {/* <View style={styles.switch}> 
      <Switch value={switchText} onValueChange={ () => setSwitchText(!switchText)} />
      </View>
      
      <View style={{ marginVertical: 20, marginHorizontal:20}}> 
      <Button title='this is button' color='crimson' onPress={() => console.log('hello')} />
      </View>
      <TextInput value={inputText} 
      style={styles.inputStyle}
      onChangeText={(value) => setInputText(value)}/>
      <TouchableOpacity 
      style={styles.button}> 
          <Text style={styles.textButton}> Click me </Text>
      </TouchableOpacity>
      <TouchableOpacity 
      style={styles.button}> 
          <Text style={styles.textButton}> Click me </Text>
      </TouchableOpacity>
      <TouchableOpacity 
      style={styles.button}> 
          <Text style={styles.textButton}> Click me </Text>
      </TouchableOpacity>
      <TouchableOpacity 
      style={styles.button}> 
          <Text style={styles.textButton}> Click me </Text>
      </TouchableOpacity> */}
    </View>
  );
  }

  const styles = StyleSheet.create({
    button: {
       backgroundColor: '#26C6DA', 
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 20,
        borderRadius: 10,
        elevation: 5,
    },
    inputStyle: { 
        borderWidth: 1,
        borderColor: 'blue',
        padding: 10,
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 20
       },
       textButton : { fontWeight: 'bold', color: 'white' },
       switch: { justifyContent: 'center', alignItems: 'center', marginTop: 30 },
       image : {width: width, height:300},
       imageContainer: { justifyContent:'center', alignItems:'center', marginTop:50 },
       textHeader : { fontWeight: 'bold', color: 'white', fontSize : 20 },
       header: { 
      backgroundColor: '#64B5F6',
      paddingVertical: 20,
      justifyCenter: 'center',
      alignItems: 'center',
      elevation: 10 },
      container: { flex: 1 },
      arrContainer: {
        backgroundColor: 'lightblue',
        padding: 10,
        marginHorizontal: 20,
        marginBottom: 20
      },
      flatListContainer : { flex: 1, marginTop: 20 }
  })

export default App;
